# Spring-boot-crud
demo api with crud operations and Authentication with JWT Tokens,  admin user with privileges, and multiLanguage error messages.  
example Unit/integration test are included.  

technologies used:  
  
-Spring boot  
-Spring security (with JWT)  
-Apache maven  
-MongoDB  
-Junit5  
-Mockito  
-Google Truth (test assertions Library)  
-Apache Poi (Excel/csv creation)  
-Mapstruct  
-Swagger2  
-Docker  
-Jacoco (code coverage) 
  
to explore the API Docs, use the following link:  
  
[swagger docs link](http://localhost:8080/crud-service/swagger-ui/)
  
todos:  
-write tests  
-add authentication  
