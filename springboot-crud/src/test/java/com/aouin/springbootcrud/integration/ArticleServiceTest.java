package com.aouin.springbootcrud.integration;

import com.aouin.springbootcrud.service.impl.ArticleServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

@Tag("integration_article")
class ArticleServiceTest extends AbstractBaseTest {

    @Autowired
    private ArticleServiceImpl articleService;

    @DisplayName("findArticleById")
    @Nested
    class findArticleById {
    }

    @DisplayName("getAllArticles")
    @Nested
    class getAllArticles {
    }

    @DisplayName("getArticlesByFilter")
    @Nested
    class getArticlesByFilter {
    }

    @DisplayName("getArticlesByCategory")
    @Nested
    class getArticlesByCategory {
    }

    @DisplayName("addArticle")
    @Nested
    class addArticle {
    }

    @DisplayName("deleteArticle")
    @Nested
    class deleteArticle {
    }
}