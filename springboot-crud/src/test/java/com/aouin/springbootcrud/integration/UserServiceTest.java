package com.aouin.springbootcrud.integration;

import com.aouin.springbootcrud.service.impl.UserServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

@Tag("integration_user")
class UserServiceTest extends AbstractBaseTest {

    @Autowired
    private UserServiceImpl userService;

    @DisplayName("findUserById")
    @Nested
    class findUserById {
    }

    @DisplayName("isEmailAlreadyUsed")
    @Nested
    class isEmailAlreadyUsed {
    }

    @DisplayName("isUsernameAlreadyUsed")
    @Nested
    class isUsernameAlreadyUsed {
    }

    @DisplayName("addUser")
    @Nested
    class addUser {
    }
}